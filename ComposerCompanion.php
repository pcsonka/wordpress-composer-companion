<?php

namespace pcsonka\composer_companion;

class ComposerCompanion
{
    public function composerFilesFound(): bool
    {
        $composerPath = get_home_path() . '/composer.json';
        $composerLockPath = get_home_path() . '/composer.lock';

        if (!file_exists($composerLockPath) || !file_exists($composerPath)) {
            return false;
        }

        return true;
    }

    public function getLockedVersion(string $name)
    {
        $composerLockPath = get_home_path() . '/composer.lock';

        if (!file_exists($composerLockPath)) {
            return false;
        }

        $composerLock = json_decode(file_get_contents($composerLockPath), true);
        foreach ($composerLock['packages'] as $package) {
            $packageName = substr($package['name'], strripos($package['name'], '/') + 1);
            if ($packageName == $name) {
                return $package['version'];
            }
        }

        return false;
    }

    public function getRequiredVersion(string $name)
    {
        $composerPath = get_home_path() . '/composer.json';

        if (!file_exists($composerPath)) {
            return false;
        }

        $composer = json_decode(file_get_contents($composerPath), true);
        foreach ($composer['require'] as $package => $packageVersion) {
            $packageName = substr($package, strripos($package, '/') + 1);
            if ($packageName == $name) {
                return $packageVersion;
            }
        }

        return false;
    }

    public function isComposerAvailable(): bool
    {
        if (!$composerAvailabily = wp_cache_get('composer_available')) {
            if (empty(shell_exec('cd ' . get_home_path() . ' && composer --quiet 2>&1'))) {
                $composerAvailabily = 'available';
            } else {
                // the command call run into an error, will get an error message, so the result wont be empty
                $composerAvailabily = 'not available';
            }
            wp_cache_set('composer_available', $composerAvailabily, '', 3600);
        }
        if ($composerAvailabily == 'available') {
            return true;
        }

        return false;
    }

    public function getNewestVersion(string $name)
    {
        if (!$this->isComposerAvailable()) {
            return false;
        }

        if (!$outdated = wp_cache_get('composer_outdated_data')) {
            $output = shell_exec('cd ' . get_home_path() . ' && composer outdated --format=json 2>&1');
            preg_match('/\{(.|\n)*\}/', $output, $match);
            $outdated = json_decode($match[0], true);

            wp_cache_set('composer_outdated_data', $outdated, '', 3600);
        }

        if (!isset($outdated['installed'])) {
            return false;
        }

        foreach ($outdated['installed'] as $package) {
            $packageName = substr($package['name'], strripos($package['name'], '/') + 1);
            if ($packageName == $name) {
                return $package['latest'];
            }
        }
    }
}