<?php
/**
 * Plugin Name: Composer companion
 * Plugin URI: http://peter.csonka.dev
 * Description: Companion plugin for wpackagist based composer so one can see the package details on the plugins UI
 * Version: 0.2
 * Author: Peter Csonka
 * Author URI: http://peter.csonka.dev
 * License: GPL2
 */

add_filter('plugin_row_meta', 'plugin_row_extension', 0, 2);

function plugin_row_extension($links, $file)
{
    require_once 'ComposerCompanion.php';
    $composerCompanion = new \pcsonka\composer_companion\ComposerCompanion();

    if (!$composerCompanion->composerFilesFound()) {
        return $links;
    }

    $filePath = substr($file, 0, strpos($file, '/'));
    if ($lockedVersion = $composerCompanion->getLockedVersion($filePath)) {
        array_splice($links, 1, 0, ['Composer lock: ' . $lockedVersion]);
    }

    if ($requiredVersion = $composerCompanion->getRequiredVersion($filePath)) {
        array_splice($links, 2, 0, ['Composer require: ' . $requiredVersion]);
    }

    if ($updateableVersion = $composerCompanion->getNewestVersion($filePath)) {
        array_splice($links, 3, 0, [
            '<strong style="color: #b32d2e">Updatable to: ' . $updateableVersion . '</strong>'
        ]);
    }

    return $links;
}
